create table weekroutenew4
as
SELECT gps.truckid, gps.date,ST_MakeLine(gps.geom ORDER BY gps.time) As geom,
min(gps.gid) as gid,
min(gps.time) as min_t, 
max(gps.time) as max_t,
avg (gps.speed) as ave_speed
FROM octweek As gps
GROUP BY gps.truckid, gps.date;
commit;