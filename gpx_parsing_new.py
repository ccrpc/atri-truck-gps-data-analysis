# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 10:42:40 2019

Author: Topi Tjukanov (https://gist.github.com/tjukanovt/9e54724221e888c2e4ac31f0bd565c91)
Modified by: Shuake Wuzhati

"""

import gpxpy
import csv   
import os
import re
import datetime

#create csv file called merged.csv to working directory and give column names x, y & t
with open(r'L:/Freight Study/Existing Condition/ATRI Truck GPS/OpenStreetMap/meijer_test_route.csv', 'a') as f:
    writer = csv.writer(f, quoting=csv.QUOTE_NONE, escapechar=' ', lineterminator='\n')
    writer.writerow('yxt')
    

for file in os.listdir('L:/Freight Study/Existing Condition/ATRI Truck GPS/OpenStreetMap/meijer_test_pgx_files_timechange_output/'):
    filepath = 'L:/Freight Study/Existing Condition/ATRI Truck GPS/OpenStreetMap/meijer_test_pgx_files_timechange_output/' + file
    gpx_file = open(filepath, 'r')
    gpx = gpxpy.parse(gpx_file)
    count = 0
    
    #iterate through rows and append each gpx row to merged csv
    for track in gpx.tracks:
        for segment in track.segments:
            for point in segment.points:
                point.time=point.time+datetime.timedelta(hours=delay)
                fields=['{0},{1},{2}'.format(point.latitude, point.longitude, point.time)] 
                re.sub(' +',' ',fields[0])
                count += 1
                if count % 2 == 0: 
                    with open(r'L:/Freight Study/Existing Condition/ATRI Truck GPS/OpenStreetMap/meijer_test_route.csv', 'a') as f:
                        writer = csv.writer(f, quoting=csv.QUOTE_NONE, escapechar=' ', lineterminator='\n')
                        writer.writerow(fields)

filepath='L:/Freight Study/Existing Condition/ATRI Truck GPS/OpenStreetMap/meijer_test_pgx_files_timechange_output/ChampaignUS_15465_modified.gpx'
gpx_file = open(filepath, 'r')
gpx = gpxpy.parse(gpx_file)
