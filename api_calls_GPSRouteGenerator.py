# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 10:30:12 2019

Author: Topi Tjukanov (https://gist.github.com/tjukanovt/9e54724221e888c2e4ac31f0bd565c91)
Modified by: Shuake Wuzhati

"""

import pandas as pd
import urllib.request

# path to your csv file with the endpoint coordinates
record = pd.read_csv('L:\\Freight Study\\Existing Condition\\ATRI Truck GPS\OpenStreetMap\\meijer_1.csv')

# graphhopper API call building blocks. Check Graphhopper documentation how to modify these. 
urlStart = 'http://localhost:8989/route?'
point = 'point='
urlEnd = '&type=gpx&instructions=false&vehicle=car'
separator = '%2C'

# The starting point for each query (lat, lon). Use e.g. QGIS to change these. 
  
for row, nextrow in zip(record.iterrows(),record.iloc[1:].iterrows()):
    startY=str(row[1]['y'])
    startX=str(row[1]['x'])

    endY=str(nextrow[1]['y'])
    endX=str(nextrow[1]['x'])
    
    req = urlStart + point + startY + separator + startX + '&' + point + endY + separator + endX + urlEnd
    print(req)
    try:
        resp = urllib.request.urlopen(req)
        gpxData  = str(resp.read(),'utf-8')
        fileName = 'ChampaignUS_' + str(row[1][1])
        saveFile = open('L:/Freight Study/Existing Condition/ATRI Truck GPS/OpenStreetMap/meijer_1_gpx_files/{0}.gpx'.format(fileName), 'w')
        print('processed index ' + str(row[1][1]))
        saveFile.write(gpxData)
        saveFile.close()
    except: 
        print('bad request on index ' + str(row[1][1]))
        pass
