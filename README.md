# ATRI Truck GPS Data Analysis

## Descriptive analysis
GPSAnalysis.R analyzes number of unique trucks, number of truck trips, daily and hourly distribution of truck trips, and trip origin and destination identification.

## Create truck movement routes from truck GPS points
makeline.sql uses ST_MakeLine to create truck movement routes from truck GPS points.  
![Routes of trucks serving a delivery_company](Routes_of_trucks_serving_a_delivery_company.png)

## Animated route with Graphhopper and QGIS
Implementation of [tutorial by Topi Tjukanov](https://medium.com/@tjukanov/animated-routes-with-qgis-9377c1f16021).

*  api_calls_GPSRouteGenerator.py: creates GPX files from GPS records
*  gpx_parsing_new.py: converts route GPX files to a single csv to be used in QGIS TimeManager plugin to create GIFs.

### Animated major truck routes originated from Champaign County 
![Champaign County to rest of Illinois](GIFs/Illinois.gif)

### Animated truck GPS points in Champaign County with green representing higher speed and red representing slower speed
![Points](GIFs/TruckSpeed_2.gif)

### Animated truck GPS routes in Champaign County
![Routes](GIFs/TruckTrajectorye.gif)

## Detailed analysis for 9 areas of interest using PostgreSQL
AreaOfInterestProcess.sql

