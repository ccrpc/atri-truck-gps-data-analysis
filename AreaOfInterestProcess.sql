create table meijer_weeknew as
select to_char(to_date(date, 'MM/DD/YYYY'), 'YYYY-MM-DD'), gid, truckid
from meijer_week;
commit;

SELECT gps.gid,
	gps.date,
	gps.time,
	gps.y,
	gps.x,
	gps.speed,
	gps.truckid,
    gps.geom
FROM octweek AS gps
INNER JOIN meijer_weeknew AS InterestLocation
    ON InterestLocation.truckid = gps.truckid
    AND InterestLocation.to_char = gps.date;
	
commit;

select truckid, count(gid) from octweek_meijer group by truckid order by truckid asc
select truckid, count(gid) from meijer_weeknew group by truckid order by truckid asc

alter table weekroute
alter column truckid type varchar;
commit;

CREATE table weekroute_meijer as
SELECT route.gid,
	route.truckid,
	route.date,
	route.min_t,
	route.max_t,
	route.ave_speed,
	route.geom
FROM weekroutenew3 AS route
INNER JOIN meijer_weeknew AS InterestLocation
    ON InterestLocation.truckid = route.truckid
    AND InterestLocation.to_char = route.date;
commit;